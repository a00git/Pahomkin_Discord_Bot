import { Client, Message, RichEmbed } from 'discord.js';
// import { Database } from 'sqlite3';
import * as dotenv from 'dotenv';
import * as BotCommands from './bot_commands/BotCommands';

dotenv.config({path: '../config.env'}); // no need to try..catch

export interface IEmbedField {
	title: string;
	body: string;
};

export interface IEmbedFields extends Array<IEmbedField>{};

export class PahomkinBot {
	private _client: Client;
	private _prefix: string;
	private _owner_id: string;
	private _token: string;

	public get client() : Client {
		return this._client;
	}

	public get prefix() : string {
		return this._prefix;
	};

	public get owner_id() : string {
		return this._owner_id;
	}

	public get token() : string {
		return this._token;
	}

	constructor() {
		this._client = new Client();
		this._token = process.env.TOKEN;
		this._prefix = process.env.PREFIX;
		this._owner_id = process.env.OWNERID;

		this._client.login(this._token);
		this._client.on('ready', () => this.setup())
	}

	private setup() {
		BotCommands.command_ping(this);
		BotCommands.command_setgame(this);
		BotCommands.command_sendembed(this);

		this.client.user.setGame('Discord.js');
		console.log(`Logged in as ${ this._client.user.username } (${ this._client.user.tag})`);
	}

	public sendEmbed(fields: IEmbedFields) {
		const embed = new RichEmbed()
			.setColor(0x00AE86)
			.setTimestamp();

		for (var i = fields.length - 1; i >= 0; i--) {
			embed.addField(fields[i].title, fields[i].body);
		}

		this._client.channels.get(process.env.GENERALCHANNEL).sendEmbed(embed);
	}
}
