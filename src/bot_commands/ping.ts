import { Client } from 'discord.js';
import { PahomkinBot } from '../PahomkinBot';

export default function (bot: PahomkinBot) {
	bot.client.on('message', msg => {
		const command: string = 'ping';

		if (msg.content == process.env.PREFIX + command) {
			msg.channel.send('Pong to you, Mr.' + msg.author.username);
		}
	});
};
