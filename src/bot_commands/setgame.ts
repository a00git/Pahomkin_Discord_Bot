import { Client } from 'discord.js';
import { PahomkinBot } from '../PahomkinBot';

export default function (bot: PahomkinBot) {
	bot.client.on('message', msg => {
		const command: string = 'setgame';

		if (msg.author.id == bot.owner_id && msg.content.startsWith(bot.prefix + command)) {
			var game_title = msg.content.replace(bot.prefix + command, '');
			bot.client.user.setGame(game_title);
		}
	});
};
